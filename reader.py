import numpy as np
from simple_caching import NpyFS
from nwdata import StaticBatchedReader, MergeBatchedReader, DebugReader, RandomIndexReader, BuilderReader
from ngclib.readers import NGCNpzReader
from nwmodule.graph import Edge

def mergeFn(x):
	Keys = x[0]["data"].keys()
	res = {k : np.array([y["data"][k] for y in x], dtype=np.float32) for k in Keys}
	# Input is RGB only for this paper. Either RGB->XXX (single link) or RGB->XXX->YYY (two hops)
	data = {"rgb":res["rgb"]}
	return {"data":data, "labels":res}

def getReader(args, model, path):
	reader = NGCNpzReader(path, model.getNodes())
	if args.debug:
		reader = DebugReader(reader, N=50)
	# reader = CachedReader(reader, cache=NpyFS)
	reader = RandomIndexReader(reader, seed=42)
	reader = MergeBatchedReader(reader, mergeFn)
	return reader
