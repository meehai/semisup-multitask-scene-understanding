import numpy as np
import torch as tr
import yaml
from tqdm import trange
from argparse import ArgumentParser
from nwdata.utils import changeDirectory, fullPath
from ngclib.models import *
from ngclib.readers import NGCImagePlotter, EdgeReader

from model import *
from reader import getReader

import torch.optim as optim
from nwmodule.callbacks import SaveModels, SaveHistory, PlotMetrics, Callback, RandomPlotEachEpoch
from nwmodule.schedulers import ReduceLRAndBacktrackOnPlateau
from nwmodule.utilities import npGetData
from functools import partial
from nwdata import StaticBatchedReader
from media_processing_lib.image import tryWriteImage

tr.backends.cudnn.deterministic = True
tr.backends.cudnn.benchmark = False

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("--ngcDir", required=True, \
		help="Path to the NGC directory containing the model exported files.")
	parser.add_argument("--cfgPath", required=True, \
		help="Path to YAML of the graph containing structure/edges etc.")
	# Reader stuff
	parser.add_argument("--datasetPath", required=True)
	parser.add_argument("--debug", type=int, default=0)
	parser.add_argument("--validationDatasetPath")
	# Training stuff
	parser.add_argument("--trainCfgPath")
	parser.add_argument("--currentEdge")
	# Export pseudolabels stuff
	parser.add_argument("--pseudolabelsDir", help="Path to the new NGC directory to export pseudolabels to")

	args = parser.parse_args()
	args.ngcDir = fullPath(args.ngcDir)
	args.datasetPath = fullPath(args.datasetPath)
	args.validationDatasetPath = None if args.validationDatasetPath is None else fullPath(args.validationDatasetPath)
	args.debug = bool(args.debug)

	args.cfg = yaml.safe_load(open(args.cfgPath, "r"))
	if args.type in ("train", "retrain"):
		assert not args.trainCfgPath is None
		assert not args.currentEdge is None
		args.trainCfg = yaml.safe_load(open(args.trainCfgPath, "r"))
	if args.type == "export_pseudolabels":
		assert not args.pseudolabelsDir is None
		args.pseudolabelsDir = fullPath(args.pseudolabelsDir)
		assert not args.pseudolabelsDir.exists()

	# Water the plants
	return args

def edgePlotFn(x, y, t, edge, rgb):
	cnt = 0
	MB = len(t)
	A, B = edge.getNodes()
	if isinstance(edge, TwoHopsNoGT):
		x = edge.singleLinkEdge.npForward(x)
	else:
		A = rgb

	for i in range(MB):
		xImage = NGCImagePlotter()(A, x[i])
		yImage = NGCImagePlotter()(B, y[i])
		tImage = NGCImagePlotter()(B, t[i])

		stack = np.concatenate([xImage, yImage, tImage], axis=1)
		tryWriteImage(stack, "%d.png" % cnt)
		cnt += 1

def setupEdge(trainCfg, edge, rgb):
	print("[setupEdge] Setting up train cfg: %s" % trainCfg)
	edge.setTrainableWeights(True)
	if isinstance(edge, TwoHopsNoGT):
		edge.singleLinkEdge.setTrainableWeights(False)
	assert edge.getNumParams()[1] > 0, "Edge '%s' has no trainable params." % edge

	# Optimizer
	optimizerType = {
		"adam" : optim.Adam,
		"sgd" : optim.SGD,
		"rmsprop" : optim.RMSprop,
		"adamw" : optim.AdamW
	}[trainCfg["optimizerType"]]
	optimizerType = partial(optimizerType, **trainCfg["optimizerArgs"])
	edge.setOptimizer(optimizerType)

	# Scheduler
	if trainCfg["patience"] > 0:
		scheduler = ReduceLRAndBacktrackOnPlateau(edge, "Loss", trainCfg["patience"], trainCfg["factor"])
		edge.setOptimizerScheduler(scheduler)

	# Callbacks
	edge.addCallbacks([
		SaveHistory("history.txt"), \
		SaveModels("best", "Loss"), \
		SaveModels("last", "Loss"), \
		PlotMetrics(["Loss"]), \
		RandomPlotEachEpoch(partial(edgePlotFn, edge=edge, rgb=rgb))
	])

def main():
	args = getArgs()
	model = getModel(args)
	print(model.summary())
	reader = getReader(args, model, args.datasetPath)
	valReader = None if args.validationDatasetPath is None else getReader(args, model, args.validationDatasetPath)
	print(reader)
	changeDirectory(args.ngcDir)

	if args.type in ("train", "retrain"):
		model.train()
		np.random.seed(args.trainCfg["seed"])
		tr.manual_seed(args.trainCfg["seed"])
		edge = model.getEdge(args.currentEdge)
		setupEdge(args.trainCfg, edge, model.ngcNodes["rgb"])
		if args.type == "train":
			assert not model.isEdgeTrained(args.currentEdge), "Edge '%s' is already trained" % args.currentEdge
		else:
			edge.loadModel(args.ngcDir / args.currentEdge / "model_last.pkl")

		trainGenerator = EdgeReader(edge, StaticBatchedReader(reader, \
			batchSize=args.trainCfg["batchSize"]), inKey="rgb").iterate()
		validationGenerator = None if valReader is None else EdgeReader(edge, StaticBatchedReader(valReader, \
			batchSize=args.trainCfg["batchSize"]), inKey="rgb").iterate()

		Dir = args.ngcDir / args.currentEdge
		changeDirectory(Dir)
		edge.trainGenerator(trainGenerator, args.trainCfg["numEpochs"], validationGenerator)
	elif args.type == "test":
		assert sum(model.loaded) == len(model.edges), "Not all edges are trained!. %d vs %d" % \
			(sum(model.loaded), len(model.edges))
		model.eval()
		reader = StaticBatchedReader(reader, batchSize=10)
		edgeResults = {}
		# Comment this block of code if you don't care about single links performance.
		for edge in model.edges:
			y = edge.testGenerator(EdgeReader(edge, reader).iterate())
			edgeResults[edge] = y

		print("Edge metrics")
		for edge in edgeResults:
			print("\t- %s: %s" % (edge, edgeResults[edge]))

		yNode = model.testGenerator(reader.iterate())
		print("Node metrics")
		for node in yNode["Test"]["nodes"]:
			print("\t- %s: %s" % (node, yNode["Test"]["nodes"][node]))
	elif args.type == "export_pseudolabels":
		assert sum(model.loaded) == len(model.edges), "Not all edges are trained!. %d vs %d" % \
			(sum(model.loaded), len(model.edges))
		model.eval()
		args.pseudolabelsDir.mkdir(parents=True, exist_ok=False)
		for i in trange(len(reader), desc="Export Pseudolabels"):
			x = reader[i]["data"]
			y = model.npForward(x)
			for node in y:
				outDir = args.pseudolabelsDir / node.groundTruthKey
				outDir.mkdir(exist_ok=True)
				if node.groundTruthKey == "rgb":
					nodeOutput = x["rgb"][0]
				else:
					messages = y[node]
					assert len(messages) == 1 and messages[0].path == ["Reduce"]
					message = messages[0]
					nodeOutput = message.output[0]
					if isinstance(node, Semantic):
						nodeOutput = (nodeOutput == nodeOutput.max(axis=-1, keepdims=True)).astype(np.uint8)
				outFilePath = outDir / ("%d.npz" % i)
				np.savez_compressed(outFilePath, nodeOutput)
	else:
		assert False, "Unknown type: %s" % args.type

if __name__ == "__main__":
	main()
