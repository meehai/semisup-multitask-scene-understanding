import torch as tr
from ngclib.nodes import Depth as _Depth
from nwmodule.graph import MapNode, Message
from ..Map2Map import EncoderMap2Map, DecoderMap2Map

class Depth(_Depth):
	def getEncoder(self, outputNode):
		assert isinstance(outputNode, MapNode)
		return EncoderMap2Map(dIn=self.numDims)

	def getDecoder(self, inputNode):
		assert isinstance(inputNode, MapNode)
		return DecoderMap2Map(dOut=self.numDims)

	def aggregate(self):
		messages = self.getMessages()
		newPath = ["Reduce"]
		newInput = tr.stack([x.output.detach() for x in messages], dim=0)
		newOutput = sum(newInput) / len(newInput)
		self.clearMessages()
		self.addMessage(Message(newPath, newInput, newOutput))
